#include <Adafruit_NeoPixel.h> // include the Neopixel Library in this Sketch
#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include "credentials.h"

const char ssid[] = WIFI_SSID;
const char password[] = WIFI_PASSWD;
const int colorButton = 2;
int colorButtonState = 0;
int currentColorVal = 0;
long lastColorButtonDebounceTime = 0;
long colorDebounceDelay = 500;

#define pot A0      

#define PIN 14 // This is the Arduino Pin controlling the LEDstrip.

#define NUMPIXELS 99 // Here, you are informing how many LEDs you have on your strip.
// You can also control only a part of the existing LEDs, if you wish.
// This strip has 60 LEDs, so I am informing this number.
#define FASTLED_ALLOW_INTERRUPTS 0
/*
    Remember that I keep annoying you whith capital IMPORTANT warnings? This next line is where it really matters.
*/
const long utcOffsetInSeconds = -18000;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800); // Here, you are specifying your strip,
//Adafruit_NeoPixel strip = Adafruit_NeoPixel(99, PIN, NEO_GRB + NEO_KHZ800);
WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);




struct digit {
  int top[3];
  int topRight[3];
  int topLeft[3];
  int middle[3];
  int bottom[3];
  int bottomRight[3];
  int bottomLeft[3];
};

struct hours {
  struct digit tens;
  struct digit ones;
};

struct minutes {
  struct digit tens;
  struct digit ones;
};


struct separator {
  int top[3];
  int bottom[3];
};




struct digit hourTensDigit {

  .top = {16, 17, 18},
   .topRight = { 19, 20, 21},
    .topLeft = {13, 14, 15 },
     .middle = {0, 1, 2},
      .bottom = {6, 7, 8 },
       .bottomRight = {3, 4, 5 },
        .bottomLeft = { 9, 10, 11},

};



struct digit hourOnesDigit {

  .top = {28, 29, 30},
   .topRight = { 31, 32, 33},
    .topLeft = {25, 26, 27},
     .middle = {44, 45, 46},
      .bottom = {38, 39, 40 },
       .bottomRight = {35, 36, 37},
        .bottomLeft = { 41, 42, 43},

};

struct hours h = {
  hourTensDigit,
  hourOnesDigit,
};


struct digit minTens  {


  .top = {68, 69, 70},
   .topRight = { 71, 72, 73},
    .topLeft = {65, 66, 67 },
     .middle = {52, 53, 54},
      .bottom = {58, 59, 60 },
       .bottomRight = {55, 56, 57 },
        .bottomLeft = { 61, 62, 63},
};


struct digit minOnes {


  .top = {80, 81, 82},
   .topRight = { 83, 84, 85},
    .topLeft = {77, 78, 79},
     .middle = {96, 97, 98},
      .bottom = {90, 91, 92 },
       .bottomRight = {87, 88, 89},
        .bottomLeft = { 93, 94, 95},


};



struct minutes m = {
  minTens,
  minOnes,
};


struct separator s = {
  .top = {50, 51},
  .bottom = {47, 48},
};

struct ledClock {
  struct hours hrs;
  struct separator sep;
  struct minutes mins;
};

struct ledClock clk = {
  .hrs = h,
  .sep = s,
  .mins = m,
};

void wifiText(String color ) {
  int wifiPixels[46] = {13, 14, 15, 9, 10, 11, 6, 7, 8, 3, 4, 5, 41, 42, 43, 38, 39, 40, 35, 36, 37, 31, 32, 33, 50, 51, 47, 48, 65, 66, 67, 61, 62, 63, 68, 69, 70, 52, 53, 54, 77, 78, 79, 93, 94, 95};

  int r = 0;
  int g = 0;
  int b = 0;

  if (color == "red") {

    r = 255;
  } else if (color == "blue") {
    b = 255;
  } else if  (color == "green") {
    g = 255;

  }
  for (int i = 0; i < 46; i++) {

    pixels.setPixelColor(wifiPixels[i], r, g, b);
  }
  pixels.show();


}

void setupWiFi(){

  WiFi.begin(ssid, password);
  int retries = 0;
  while ((WiFi.status() != WL_CONNECTED) && (retries < 15)){ retries++; delay(500); Serial.print("."); } if(retries>14){
    Serial.println(F("WiFi conenction FAILED"));
  }
  if (WiFi.status() == WL_CONNECTED){
    Serial.println(F("WiFi connected!"));
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
  Serial.println(F("Setup ready"));
}

void separatorOn(struct separator s, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(s.top[j], Wheel((j + i) & 255));
    pixels.setPixelColor(s.bottom[j], Wheel((j + i) & 255));
    pixels.setPixelColor(0, 0);

  }
  pixels.show();
}

void searatorOff(struct separator s) {

  for (int i = 0; i < 3; i++) {
    pixels.setPixelColor(s.top[i], 0, 0, 0);
    pixels.setPixelColor(s.bottom[i], 0, 0, 0);

  }
}


int delayval = 50; // Here we set a delaytime
int currHours = 0;
int currMins = 0;

//const int colorButton = D0;
//int colorButtonState = 0;
//int currentColorVal = 0;
//long lastColorButtonDebounceTime = 0;
//long colorDebounceDelay = 50;


void setup() {
  

pinMode(colorButton, INPUT_PULLUP);


  Serial.begin(115200);
  delay(300);
  


  pixels.begin();
  pixels.setBrightness(80);
  wifiText("red");

  setupWiFi();

  wifiText("green");
  timeClient.begin();
  timeClient.update();

  delay(1000);
  // Print the IP address


pinMode(pot, INPUT);



  clear();
  setTime(convertHour(timeClient.getHours()), timeClient.getMinutes(), 1);


}


int loopCounter = 0;
int countDirection = 0; // 0 = up  1 = down;
int currHour = 0;
int currMin = 0;
int convertHour(int hrs) {
  if (hrs > 12) {
    return hrs - 12;
  }
  if (hrs == 0) {
    return 12;
  }

  return hrs;
}
//const int colorButton = D0;
//int colorButtonState = 0;
//int currentColorVal = 0;
//long lastColorButtonDebounceTime = 0;
//long colorDebounceDelay = 50;
void loop() {
  //delay(500);

  colorButtonState = digitalRead(colorButton);
  
  if (colorButtonState == LOW) {
    if (currentColorVal > 255){

      currentColorVal = 0;
      } else {
                currentColorVal++;


      }

  
  }
  int currPotValue = analogRead(pot);
  currPotValue = map(currPotValue, 0, 1024, 0, 255);
  if( currPotValue > 255){
    currPotValue = 255;
  }
  if (currPotValue < 0){
    currPotValue = 0;
  }
  
    pixels.setBrightness(currPotValue);
    pixels.show();
  timeClient.update();


  //Serial.printf("delayCounter: %d   loopCounter:  %d   countDirection:  %d\n", delayCounter, loopCounter, countDirection);


  // Serial.printf("loopCounter: %d  countDirection: %d     hour: %d   min: %d\n", loopCounter, countDirection, convertHour(timeClient.getHours()), timeClient.getMinutes());
  setTime(convertHour(timeClient.getHours()), timeClient.getMinutes(), currentColorVal);
  separatorOn(s, (currentColorVal));


  delay(10);
}


void setTime(int hr, int mn, int i) {
  int ones = 0;
  int tens = 0;
  if (hr > 12) {
    return;
  }
  if (hr > 9) {
    // two digits
    ones = hr % 10;
    tens = hr / 10;

  }

  setHour(tens, hr,  i);

  int onesMin = 0;
  int tensMin = 0;

  if (mn > 59) {
    return;
  }
  if (mn > 9) {
    // two digits
    onesMin = mn % 10;
    tensMin = mn / 10;
  } else {
    onesMin = mn;
  }
  setMin(tensMin, onesMin,  i);
}







void displayNumber(int num, digit d, int i) {


  switch (num) {

    case 0:
      zero(d,  i);
      break;

    case 1:

      one(d,  i);
      break;

    case 2:

      two(d,  i);
      break;

    case 3:

      three(d,  i);
      break;

    case 4:

      four(d,  i);
      break;

    case 5:

      five(d,  i);
      break;

    case 6:

      six(d,  i);
      break;

    case 7:

      seven(d,  i);
      break;

    case 8:

      eight(d,  i);
      break;

    case 9:

      nine(d,  i);
      break;

  }



}

void setHour(int tens, int ones, int i) {
  if (tens == 0) {
    clearDigit(clk.hrs.tens);
  } else {
    displayNumber(tens, clk.hrs.tens,  i);
  }
  displayNumber(ones, clk.hrs.ones,  i);

}


void setMin(int tens, int ones,  int i) {

  displayNumber(tens, clk.mins.tens,  i);

  displayNumber(ones, clk.mins.ones,  i);

}



void zero(struct digit d,  int i) {
  clearDigit(d);
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], 0);
  }
  pixels.show();

}

void one(struct digit d,  int i) {

  for (int j = 0; j < 3; j++) {
    // pixels.setPixelColor(d.topRight[j], pixels.Color(r,g,b));
    pixels.setPixelColor(d.topRight[j], Wheel((i + j) & 255));

  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((i + j) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], 0, 0, 0);
  }
  pixels.show();

}

void two(struct digit d, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], Wheel((j + i) & 255));
  }


  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], 0, 0, 0);
  }
  pixels.show();

}

void three(struct digit d, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], 0, 0, 0);
  }          for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], 0, 0, 0);
  }
  pixels.show();
}

void four(struct digit d, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], 0, 0, 0);
  }
  pixels.show();

}

void five(struct digit d, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], 0, 0, 0);
  }
  pixels.show();

}

void six(struct digit d, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], Wheel((j + i) & 255));
  }


  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], Wheel((j + i) & 255));

  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], 0, 0, 0);
  }
  pixels.show();

}
void seven(struct digit d, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], Wheel((j + i) & 255));
  }


  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], 0, 0, 0);
  }
  pixels.show();


}
void eight(struct digit d, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], Wheel((j + i) & 255));
  }

  pixels.show();

}
void nine(struct digit d, int i) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], Wheel((j + i) & 255));
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], Wheel((j + i) & 255));
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], 0, 0, 0);
  }

  pixels.show();

}

void clearDigit(struct digit d) {

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topRight[j], 0, 0, 0);
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.topLeft[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.top[j], 0, 0, 0);
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.middle[j], 0, 0, 0);
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomRight[j], 0, 0, 0);
  }

  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottom[j], 0, 0, 0);
  }
  for (int j = 0; j < 3; j++) {
    pixels.setPixelColor(d.bottomLeft[j], 0, 0, 0);
  }
  pixels.show();


}

void clear() {
  for (int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, 0);

  }
  pixels.show();


}



// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;

  if (WheelPos < 85) {
    return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170) {
    WheelPos -= 85;
    return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
